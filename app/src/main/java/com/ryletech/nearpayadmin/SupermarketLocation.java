package com.ryletech.nearpayadmin;

/**
 * Created by sydney on 1/9/17.
 */

public class SupermarketLocation {

private String placeId,placeName,placeAddress,placePhoneNumber;
private float placeRatings;

    public SupermarketLocation() {
    }

    public SupermarketLocation(String placeId, String placeName, String placeAddress, String placePhoneNumber, float placeRatings) {
        this.placeId = placeId;
        this.placeName = placeName;
        this.placeAddress = placeAddress;
        this.placePhoneNumber = placePhoneNumber;
        this.placeRatings = placeRatings;
    }

    public String getPlaceId() {
        return placeId;
    }

    public String getPlaceName() {
        return placeName;
    }

    public String getPlaceAddress() {
        return placeAddress;
    }

    public String getPlacePhoneNumber() {
        return placePhoneNumber;
    }

    public float getPlaceRatings() {
        return placeRatings;
    }
}
