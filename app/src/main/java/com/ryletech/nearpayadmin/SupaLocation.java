package com.ryletech.nearpayadmin;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by sydney on 2/8/17.
 */

public class SupaLocation {

    private String placeId;
    private LatLng latLng;

    public SupaLocation() {
    }

    public SupaLocation(String placeId, LatLng latLng) {
        this.placeId = placeId;
        this.latLng = latLng;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }
}
